<%-- 
    Document   : QTJVIndex
    Created on : Jan 25, 2017, 4:30:43 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<jsp:include page="QTJVBanner.jsp" />
<section>
    <h2>Java Web Technologies: Section 1</h2>
    <br>
    <h4>Pair Programming Team:</h4>
    <h2><strong>Assignment 2</strong></h2>
    <h2><strong>Driver: Jodi Visser</strong></h2>
    <h2><strong>Observer: Quennie Teves</strong></h2>
    <br>
    <h4>Current Date and Time:</h4>
    <h2><strong><%= java.util.Calendar.getInstance().getTime() %></strong></h2>
</section>
<jsp:include page="QTJVFooter.jsp" />
