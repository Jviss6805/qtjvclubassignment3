<%-- 
    Document   : QTJVFooter
    Created on : Jan 25, 2017, 5:26:17 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<%@page import="java.util.Calendar"%>
<footer>
    <p>&copy; Copyright 
        <%= Calendar.getInstance().get(Calendar.YEAR)%> 
        Quennie Teves & Jodi Visser </p>
</footer>
</body>
</html>