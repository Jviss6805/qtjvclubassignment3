<%-- 
    Document   : QTJVError.jsp
    Created on : Feb 22, 2017, 4:34:20 PM
    Author     : Quennie Teves & Jodi Visser
--%>

<jsp:include page="QTJVBanner.jsp" /> 
    <h1>Java Error</h1>
    <p>Sorry, Java has thrown an exception.</p>
    <p>To continue, click the Back button.</p>

    <h2>Details</h2>
    <p>Type: ${pageContext.exception["class"]}</p>
    <p>Message: ${pageContext.exception.message}</p>
<jsp:include page="QTJVFooter.jsp" />
