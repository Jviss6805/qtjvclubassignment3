<%-- 
    Document   : QTJVAddBook.jsp
    Created on : February 17, 2017, 4:30:43 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="QTJVBanner.jsp" />
<section>
    <form action="QTJVAddBook" method="post">
        <h1>Add a Book</h1> 
        
        <c:if test="${!errorMessage.isEmpty()}">
            <label style="color:red; white-space:pre;">${errorMessage}</label>
        </c:if><br>
        
        <label>Code:</label>
        <input style="width:100px;" type="text" name="code" value="${code}"><br>

        <label>Description:</label>
        <input style="width:300px;" type="text" name="description" 
               value="${description}"><br>

        <label>Quantity:</label>
        <input style="width:100px;" type="text" name="quantity" 
               value="${quantity}"><br>

        <input type="submit" value="Save">
        <input type="submit" formmethod="get" formaction="QTJVDisplayBooks" 
               value="Cancel">
    </form>
</section>
<jsp:include page="QTJVFooter.jsp" />