<%-- 
    Document   : QTJVRegister
    Created on : Jan 25, 2017, 4:55:25 PM
    Authors    : Quennie Teves & Jodi Visser
--%>

<jsp:include page="QTJVBanner.jsp" />
<section>
    <h2>New Member Registration Form</h2><br>
    <form action="QTJVDisplayMember.jsp" method="post">
        <label>Full Name:</label>
        <input style="width:200px;" type="text" name="fullname" required><br>

        <label>Email:</label>
        <input style="width:300px;" type="email" name="email" required><br>

        <label>Phone:</label>
        <input style="width:100px;" type="text" name="phone"><br>

        <label>IT Program:</label>
        <select name="program" style="width: 65px">
            <option value="CAD">CAD</option>
            <option value="CP">CP</option>
            <option value="CPA">CPA</option>
            <option value="ITID">ITID</option>
            <option value="ITSS">ITSS</option>
            <option value="MSD">MSD</option>
            <option value="Others">Others</option>
        </select><br>

        <label>Year Level:</label>
        <select name="level" style="width: 30px">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
        </select><br>
        <label>&nbsp;</label>
        <input type="submit" value="Register Now!">
        <input type="reset" value="Reset">
    </form>                
</section>
<jsp:include page="QTJVFooter.jsp" />
