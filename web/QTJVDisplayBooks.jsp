<%-- 
    Document   : QTJVDisplayBooks.jsp
    Created on : Feb 15, 2017, 4:34:20 PM
    Author     : Quennie Teves & Jodi Visser
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="QTJVBanner.jsp" />
<section>
    <h2>List of Books</h2>
    <table>
        <tr>
            <th>Code</th>
            <th>Description</th>
            <th>Quantity</th>
        </tr>
        <c:forEach var="book" items="${books}">     
        <tr>
            <td><c:out value="${book.code}" /></td>
            <td><c:out value="${book.description}" /></td>
            <td><c:out value="${book.quantity}" /></td>
        </tr>       
        </c:forEach>
    </table>
    <form action="QTJVAddBook.jsp">
        <button id="addBooks" name="addBooks">Add Book</button>
    </form>
</section>
<jsp:include page="QTJVFooter.jsp" />
